import requests
from django.contrib.gis.geos import Point, LineString
from .models import Points, Lines
import networkx as nx
from rest_framework.views import APIView
from django.http import JsonResponse

answer = requests.get('https://datum-test-task.firebaseio.com/api/lines-points.json')


# Добавление данных в таблицу points
def insert_points():
    for element_point in answer.json()['points']:
        obj_id = element_point['obj_id']
        latitude = element_point['lat']
        longitude = element_point['lon']
        score = element_point['score']
        data = Points(id=obj_id, geom=Point(longitude, latitude), score=score)
        data.save()


# Добавление данных в таблицу lines
def insert_lines():
    for element_line in answer.json()['lines']:
        from_id = element_line['from_obj']
        to_id = element_line['to_obj']
        from_point = Points.objects.get(id=from_id)
        to_point = Points.objects.get(id=to_id)
        responce = Lines(from_point=from_point, to_point=to_point)
        responce.save()


# Добавление расстояния между точками в таблицу lines в поле distance
def distance_points():
    for element in Lines.objects.all():
        point_1 = element.from_point
        point_2 = element.to_point
        pnt1 = Points.objects.get(id=point_1.id).geom
        pnt2 = Points.objects.get(id=point_2.id).geom
        line = LineString(pnt1, pnt2, srid=4326)
        line.transform(3857)
        dist = line.length / 1000
        element.distance = dist
        element.save()


# Добавление баллов между точками в таблицу lines в поле distance_score
def distance_score():
    for element in Lines.objects.all():
        point_1 = element.from_point.score
        point_2 = element.to_point.score
        dist_score = point_1 + point_2
        element.distance_score = dist_score
        element.save()


# Нахождение самой короткой линии в км
def short_path_km(node_1, node_2):
    G = nx.Graph()
    list_nodes = []
    for elem in Points.objects.all():
        list_nodes.append(elem.id)

    G.add_nodes_from(list_nodes)

    list_arcs = []
    for element in Lines.objects.all():
        list_arcs.append((element.from_point.id, element.to_point.id, element.distance))

    G.add_weighted_edges_from(list_arcs)

    return nx.dijkstra_path_length(G, source=node_1, target=node_2)


# Нахождение линии имеющей минимальное количество баллов
def short_path_score(node_1, node_2):
    G = nx.Graph()
    list_nodes = []
    for elem in Points.objects.all():
        list_nodes.append(elem.id)
    # Добавляем вершины точек
    G.add_nodes_from(list_nodes)

    list_arcs = []
    for element in Lines.objects.all():
        list_arcs.append((element.from_point.id, element.to_point.id, element.distance))
    #
    G.add_weighted_edges_from(list_arcs)

    sp = nx.dijkstra_path(G, node_1, node_2)

    score_dist = sum([Points.objects.get(id=elem).score for elem in sp])
    return score_dist


class ResultPathLingth(APIView):
    def get(self, request, node_1, node_2):
        node_1 = int(node_1)
        node_2 = int(node_2)
        return JsonResponse({'result': '{}'.format(short_path_km(node_1, node_2))})


class ResultPathScore(APIView):
    def get(self, request, node_1, node_2):
        node_1 = int(node_1)
        node_2 = int(node_2)
        return JsonResponse({'result': '{}'.format(short_path_score(node_1, node_2))})
