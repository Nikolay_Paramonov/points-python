from django.contrib.gis.db import models


class Points(models.Model):
    id = models.AutoField(primary_key=True)
    geom = models.PointField()
    score = models.IntegerField()


class Lines(models.Model):
    from_point = models.ForeignKey(Points, related_name='from_geom', on_delete=models.CASCADE)
    to_point = models.ForeignKey(Points, related_name='to_geom', on_delete=models.CASCADE)
    distance = models.FloatField(blank=True, null=True)
    distance_score = models.IntegerField(blank=True, null=True)
