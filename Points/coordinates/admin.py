from django.contrib.gis import admin

from .models import Points, Lines


admin.site.register(Points, admin.GeoModelAdmin)

admin.site.register(Lines, admin.GeoModelAdmin)
