from django.urls import re_path
from .views import ResultPathLingth, ResultPathScore

urlpatterns = [
    re_path(r'^points/(?P<node_1>\d+)/min_length/(?P<node_2>\d+)/$', ResultPathLingth.as_view()),
    re_path(r'^points/(?P<node_1>\d+)/min_score/(?P<node_2>\d+)/$', ResultPathScore.as_view()),
    ]
