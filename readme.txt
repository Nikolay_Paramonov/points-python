In the views.py file
- call the insert_points () function to add data to the points table

- call the insert_lines () function to add data to the lines table

- call the distance_points () function to add the distance between points in the lines table in the distance field

- call the distance_score () function to add points between points in the lines table in the distance_score field

- To search for the shortest line:
Start the local server and in the address bar specify http://127.0.0.1:8000/api/points/node_1/min_length/node_2/
node_1 and node_2 - replace with start and end point

- To search for a line with a minimum number of points:
Start the local server and in the address bar specify http://127.0.0.1:8000/api/points/node_1/min_score/node_2/
node_1 and node_2 - replace with start and end point
